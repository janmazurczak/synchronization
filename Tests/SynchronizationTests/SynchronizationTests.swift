import XCTest
@testable import Synchronization
@testable import SynchronizationCloudKit

final class SynchronizationTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(1, 1)
    }
    
    func testRecordNameShortening() {
        let full = SynchronizationCloudKit.RecordIdentifier.prefixedWithCoordinator.value(for: "ABCD", in: "Coordinator")
        XCTAssertEqual(full.recordName, "CoordinatorABCD")
        let nonASCI = SynchronizationCloudKit.RecordIdentifier.prefixedWithCoordinator.value(for: "AB🕺CD", in: "Coordinator")
        XCTAssertEqual(nonASCI.recordName, "CoordinatorAB%F0%9F%95%BACD")
        let pathElement = SynchronizationCloudKit.RecordIdentifier.prefixedWithCoordinator.value(for: "AB/CD", in: "Coordinator")
        XCTAssertEqual(pathElement.recordName, "CoordinatorAB-CD")
        let tooLongString = "ABCDEFGHIJKLMNOPRQSTWVXYZABCDEFGHIJKLMNOPRQSTWVXYZABCDEFGHIJKLMNOPRQSTWVXYZABCDEFGHIJKLMNOPRQSTWVXYZABCDEFGHIJKLMNOPRQSTWVXYZABCDEFGHIJKLMNOPRQSTWVXYZABCDEFGHIJKLMNOPRQSTWVXYZABCDEFGHIJKLMNOPRQSTWVXYZABCDEFGHIJKLMNOPRQSTWVXYZABCDEFGHIJKLMNOPRQSTWVXYZABCDEF"
        XCTAssertEqual(tooLongString.count, 256)
        let tooLong = SynchronizationCloudKit.RecordIdentifier.prefixedWithCoordinator.value(for: tooLongString, in: "Coordinator")
        XCTAssertEqual(tooLong.recordName, "8325d7901c00449d8408172a9eb2a2f51719f4f1b0e948955c75cf42bdc7bf0c3003e0c9a6bd9a814c835f179196df57d1a85e36b4572332c6703ff15a37855f")
        XCTAssertEqual(tooLong.recordName.count, 128)
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
