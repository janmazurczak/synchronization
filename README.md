# Synchronization

First configure remotes and define coordinators for specific types:
```
import CloudKit
import Synchronization
import SynchronizationCloudKit

extension Synchronization: SynchronizationSetup {
    
    static let cloudKitRemote = SynchronizationCloudKit(
        recordTypeName: .common,
        recordIdentifier: .prefixedWithCoordinator,
        database: CKContainer.default().privateCloudDatabase)
    
    public func available(coordinators: inout AvailableCoordinators) {
        coordinators.add(\.coordinatorA)
        coordinators.add(\.coordinatorB)
    }
    
    var coordinatorA: CoordinatorDefinition<SomeCodableTypeA> {
        .init(
            named: "CoordinatorAName",
            syncing: [Self.cloudKitRemote])
    }
    
    var coordinatorB: CoordinatorDefinition<SomeCodableTypeB> {
        .init(
            named: "CoordinatorBName",
            syncing: [Self.cloudKitRemote],
            with: SynchronizationSettings(
                mergeRule: .custom({ items in
                    let sortedItems = items.sorted(by: { $0.modificationDate > $1.modificationDate })
                    
                    guard let newer = sortedItems.first else { return .none }
                    let older = sortedItems.suffix(from: 1)
                    
                    if needsMixing {
                        let item = SomeCodableTypeB(mixing: newer, older)
                        return .mixed(item, Date())
                    } else {
                        return .selected(newer)
                    }
                })
            )
        )
    }
    
}
```

Get synchronizer for each item you use:
```
// Create synchronizer reference. You can do it in multiple places and use them simultaneously even for the same type and identifier.
let synchronizedItem = Synchronized(\.coordinatorA, "itemidentifier")

// If you want to be notified when item has changed (from remote or locally) there is a Combine publisher for you
synchronizedItem.publisher

// Property wrapper in your flow is very welcome
@Synchronized(\.coordinatorA, "itemIdentifier") var item

// with property wrapper you can use item directly but you can also get access to synchronizer's functions with $
// so for example your property wrapper can also give you a publisher:
$item.publisher

// and if you are not sure about identifier to load at flow creation:
@Synchronized(\.coordinatorA) var item
// and then:
$item.load("itemIdentifier")
```

Trigger pulling changes from remote:
```
override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    $item.fetch()
}
```

And use these synchronised items like this:
```
// Create default item, which will be replaced if something newer appears on remote (Depending on coordinator's merging policy).
$item.update(item: defaultitem, modified: .distantPast)

// Modify item
$item.modify(notifyObservers: false) { // or true if you want to react locally for this change
    $0?.mutate()
    $0?.valueA = a
}

// Modify the item like:
$item.update(item: newitem, modified: Date())

// or:
item = newitem

// If item is a struct you can change only a specific value and it'll be synchronized as well:
item?.valueA = a

// Property wrappers rock, right? but all these are also available with dynamically instantiated Synchronized(\.corodinatorA, "itemIdentifier")

// Each option above will put the item in backlog for synchronization and trigger pushing changes to the remote.
```

If you use CloudKit and want to receive live updates from the other device: register to push notifications and handle them with this:
```
func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        Synchronization.cloudKitRemote.handle(notification: userInfo) {
            completionHandler($0 ? .newData : .failed)
        }
    }
```