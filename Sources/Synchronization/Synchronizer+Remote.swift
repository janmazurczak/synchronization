//
//  Synchronizer+Remote.swift
//  
//
//  Created by Jan Mazurczak on 02/08/2020.
//

import Foundation

public enum SynchronizerError: Error {
    case notFetchedYet
    case newerRemoteModel
    case missingPushHandling
    case unknownFetchingProblem
}

internal extension Synchronizer {
    
    class Remote {
        let name: String
        var status: SynchronizerRemoteStatus = .notFetchedYet
        var runtimeCache: Any?
        var item: Synchronizable<Item>?
        var pushing: UUID?
        var fetching: UUID?
        init(name: String) {
            self.name = name
        }
    }
    
    var pushExpectingRemotes: [Remote] {
        guard let merged = merged.value else { return [] }
        return remotes.filter {
            if merged.uuid == $0.item?.uuid { return false }
            switch $0.status {
            case .nothingToFetch, .canNotParse, .fetched:
                return true
            case .tooNewToParse, .notFetchedYet, .fetchingFailed:
                return false
            }
        }
    }
    
    func pushNow(to remote: Remote, completion: ((Error?) -> Void)?) {
        guard let item = merged.value else { return }
        guard let remoteCoordinator = coordinator.remote(with: remote.name) else { return }
        if item.uuid == remote.pushing { return }
        Logger.log?("Synchronizer \(identifier) is pushing to \(remote.name)")
        remote.pushing = item.uuid
        remoteCoordinator.backlog.putOnTop(identifier.filenameSafe())
        remoteCoordinator.remote.push(
            item: item,
            coordinatorID: coordinator.identifier,
            with: remote.runtimeCache
        ) { result in // keep self strong
            var pushError: Error?
            var completed = true
            self.sync {
                guard let pushing = remote.pushing, pushing == self.merged.value?.uuid else { return }
                remote.pushing = nil
                switch result.runtimeCache {
                case .unchanged: break
                case .changed(let runtimeCache): remote.runtimeCache = runtimeCache
                }
                Logger.log?("Synchronizer \(self.identifier) pushed: \(result.status) to \(remote.name)")
                switch result.status {
                case .failureRefetchIsNeeded:
                    self.fetchNow(from: remote, completion: completion)
                    completed = false
                case .failure(let error), .encodingFailure(let error):
                    pushError = error
                case .success:
                    remote.item = item
                    remoteCoordinator.backlog.remove(self.identifier.filenameSafe())
                }
            }
            if completed {
                completion?(pushError)
            }
        }
    }
    
    func fetchNow(from remote: Remote, completion: ((Error?) -> Void)?) {
        guard let remoteCoordinator = coordinator.remote(with: remote.name) else { return }
        Logger.log?("Synchronizer \(identifier) is fetching")
        let fetching = UUID()
        remote.fetching = fetching
        remoteCoordinator.remote.fetch(
            Item.self,
            identifier: identifier,
            coordinatorID: coordinator.identifier,
            modelVersion: coordinator.settings.modelVersion,
            with: remote.runtimeCache
        ) { result in // keep self strong
            var fetchError: Error?
            var completed = true
            self.sync {
                guard fetching == remote.fetching else { return }
                remote.fetching = nil
                remote.status = result.status
                switch result.runtimeCache {
                case .unchanged: break
                case .changed(let runtimeCache): remote.runtimeCache = runtimeCache
                }
                Logger.log?("Synchronizer \(self.identifier) fetched: \(result.status)")
                switch result.status {
                case .fetched, .nothingToFetch:
                    remote.item = result.item
                case .fetchingFailed(let error), .canNotParse(let error):
                    fetchError = error
                case .notFetchedYet:
                    fetchError = SynchronizerError.notFetchedYet
                case .tooNewToParse:
                    fetchError = SynchronizerError.newerRemoteModel
                }
                self.merge()
                if self.isSaveNeeded {
                    self.save()
                }
                let remotes = self.pushExpectingRemotes
                if remotes.isEmpty {
                    remoteCoordinator.backlog.remove(self.identifier.filenameSafe())
                } else {
                    let group = DispatchGroup()
                    var pushError: Error?
                    for remote in remotes {
                        group.enter()
                        self.pushNow(to: remote) { error in
                            pushError = pushError ?? error
                            group.leave()
                        }
                    }
                    group.notify(queue: DispatchQueue.main) {
                        completion?(pushError)
                    }
                    completed = false
                }
            }
            if completed {
                completion?(fetchError)
            }
        }
    }
    
}
