//
//  Filename.swift
//  
//
//  Created by Jan Mazurczak on 01/08/2020.
//

import Foundation
import CryptoKit

public extension String {
    func filenameSafe() -> String {
        var filename = self
        filename = filename.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? filename
        filename = filename.replacingOccurrences(of: "/", with: "-")
        if
            (filename.data(using: .ascii)?.count ?? 9999) > 255,
            let data = filename.data(using: .utf8)
        {
            filename = Data(SHA512.hash(data: data)).hexString().lowercased()
        }
        return filename
    }
}

private extension Data {
    func hexString() -> String {
        map { String(format: "%02.2hhx", $0) }.joined()
    }
}
