//
//  Synchronizer.swift
//
//  Created by Jan Mazurczak on 18/04/2019.
//  Copyright © 2019 Jan Mazurczak. All rights reserved.
//

import Foundation
import OnDemandDictionary
import Combine

public protocol SynchronizationSetup: Synchronization {
    func available(coordinators: inout AvailableCoordinators)
}

public final class Synchronization {
    
    private init() {
        guard let provider = self as? SynchronizationSetup else {
            Logger.log?("🛑 You should add 'extension Synchronization: SynchronizationSetup' to your project in order to use Synchronization correctly.")
            return
        }
        var availableCoordinators = AvailableCoordinators()
        provider.available(coordinators: &availableCoordinators)
        for path in availableCoordinators.paths {
            let definition = self[keyPath: path] as! AnySynchronizationCoordinatorDefinition
            let coordinator = definition.build(with: pushUpdatePublisher.eraseToAnyPublisher())
            coordinators[path] = coordinator
        }
    }
    static let shared = Synchronization()
    private var coordinators = [PartialKeyPath<Synchronization> : AnySynchronizationCoordinator]()
    
    public struct CoordinatorDefinition<Item: Codable>: AnySynchronizationCoordinatorDefinition {
        private let name: String
        private let remotes: [SynchronizationRemote]
        private let settings: SynchronizationSettings<Item>
        public init(named name: String, syncing remotes: [SynchronizationRemote] = [], with settings: SynchronizationSettings<Item> = .init()) {
            self.name = name
            self.remotes = remotes
            self.settings = settings
        }
        fileprivate func build(with pushUpdatePublisher: AnyPublisher<ExecutingRemotePushUpdate, Never>) -> AnySynchronizationCoordinator {
            let url = settings.coordinatorURL(for: name)
            let coordinator = SynchronizationCoordinator<Item>(
                identifier: name,
                url: url,
                remotes: remotes,
                settings: settings,
                pushUpdatePublisher: pushUpdatePublisher
            )
            return coordinator
        }
    }
    
    public static func coordinator<Item>(_ path: KeyPath<Synchronization, CoordinatorDefinition<Item>>) -> SynchronizationCoordinator<Item> {
        if let coordinator = shared.coordinators[path] as? SynchronizationCoordinator<Item> {
            return coordinator
        } else {
            Logger.log?("🛑 using coordinator \(path) that wasn't defined in SynchronizationSetup > available(coordinators:). The framework will create one for you this time but this is a wrong approach.")
            let definition = shared[keyPath: path]
            let coordinator = definition.build(with: shared.pushUpdatePublisher.eraseToAnyPublisher())
            shared.coordinators[path] = coordinator
            return coordinator as! SynchronizationCoordinator<Item>
        }
    }
    
    public static func handle(remotePush update: RemotePushUpdate) {
        var careTaken = false
        shared.pushUpdatePublisher
            .send(ExecutingRemotePushUpdate(update, takeCare: { careTaken = true }))
        if !careTaken { update.completion(SynchronizerError.missingPushHandling) }
    }
    private let pushUpdatePublisher = PassthroughSubject<ExecutingRemotePushUpdate, Never>()
    
}

public struct AvailableCoordinators {
    fileprivate init() {}
    fileprivate var paths = Set<PartialKeyPath<Synchronization>>()
    public mutating func add<Item>(_ path: KeyPath<Synchronization, Synchronization.CoordinatorDefinition<Item>>) {
        paths.insert(path)
    }
}

fileprivate protocol AnySynchronizationCoordinatorDefinition {
    func build(with pushUpdatePublisher: AnyPublisher<ExecutingRemotePushUpdate, Never>) -> AnySynchronizationCoordinator
}
