//
//  PersistentStack.swift
//
//  Created by Jan Mazurczak on 18/04/2019.
//  Copyright © 2019 Jan Mazurczak. All rights reserved.
//

import Foundation

class PersistentIdentifiersStack {
    
    let url: URL
    
    init(url: URL, preferredItemsPerPage: Int = 10, maxCachedPagesCount: Int = 10) {
        try! FileManager.default.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
        self.url = url
        self.syncQueue = OperationQueue()
        self.syncQueue.maxConcurrentOperationCount = 1
        self.preferredItemsPerPage = preferredItemsPerPage
        self.maxCachedPagesCount = maxCachedPagesCount
        if
            let data = try? Data(contentsOf: url.appendingPathComponent("info")),
            let info = try? decoder.decode(Info.self, from: data)
        {
            self.info = info
        } else {
            self.info = Info(lastPage: 0)
        }
    }
    
    var top: String? {
        return top(1).last
    }
    
    func top(_ max: Int) -> [String] {
        var results = [String]()
        sync {
            var pageIndex = self.info.lastPage
            while results.count < max && pageIndex >= 0 {
                let page = self.page(at: pageIndex, removeFromCache: false)
                if page.items.isEmpty && pageIndex == self.info.lastPage && pageIndex > 0 {
                    self.releaseEmptyPages()
                    pageIndex = self.info.lastPage
                    continue
                }
                results = page.items.suffix(min(page.items.count, max - results.count)) + results
                pageIndex -= 1
            }
        }
        return results
    }
    
    func putOnTop(_ identifier: String) {
        sync {
            let itemURL = self.itemURL(identifier: identifier)
            let page = self.info.lastPage
            var itemsOnLastPage = 0
            self.modifyPage(at: page) {
                $0.items = $0.items.filter { $0 != identifier } + [identifier]
                itemsOnLastPage = $0.items.count
            }
            var item: Item
            if
                let data = try? Data(contentsOf: itemURL),
                let existingItem = try? self.decoder.decode(Item.self, from: data)
            {
                item = existingItem
                if item.page != page {
                    self.modifyPage(at: item.page) {
                        $0.items = $0.items.filter { $0 != identifier }
                    }
                }
            } else {
                item = Item(id: identifier, page: -1)
            }
            if item.page != page {
                item.page = page
                guard let data = try? self.encoder.encode(item) else { return }
                try? data.write(to: itemURL)
            }
            if itemsOnLastPage >= self.preferredItemsPerPage {
                self.info.lastPage = page + 1
            }
        }
    }
    
    func remove(_ identifier: String) {
        sync {
            let itemURL = self.itemURL(identifier: identifier)
            if
                let data = try? Data(contentsOf: itemURL),
                let item = try? self.decoder.decode(Item.self, from: data)
            {
                var itemsOnPage = 0
                self.modifyPage(at: item.page) {
                    $0.items = $0.items.filter { $0 != identifier }
                    itemsOnPage = $0.items.count
                }
                try? FileManager.default.removeItem(at: itemURL)
                if itemsOnPage == 0 && item.page == self.info.lastPage {
                    self.releaseEmptyPages()
                }
            }
        }
    }
    
    // private
    
    private let encoder = JSONEncoder()
    private let decoder = JSONDecoder()
    private let preferredItemsPerPage: Int
    private let maxCachedPagesCount: Int
    private var cachedPages = [Page]()
    private var info: Info {
        didSet {
            guard let data = try? encoder.encode(info) else { return }
            try? data.write(to: url.appendingPathComponent("info"))
        }
    }
    
    private struct Item: Codable {
        let id: String
        var page: Int
    }
    
    private struct Page: Codable {
        let index: Int
        var items: [String]
    }
    
    private struct Info: Codable {
        var lastPage: Int
    }
    
    private func pageURL(at index: Int) -> URL {
        return url.appendingPathComponent("page-\(index)")
    }
    
    private func itemURL(identifier: String) -> URL {
        return url.appendingPathComponent("item-" + identifier)
    }
    
    private func page(at index: Int, removeFromCache: Bool) -> Page {
        if let i = cachedPages.firstIndex(where: { $0.index == index }) {
            if removeFromCache {
                return cachedPages.remove(at: i)
            } else {
                return cachedPages[i]
            }
        } else if
            let data = try? Data(contentsOf: pageURL(at: index)),
            let item = try? decoder.decode(Page.self, from: data)
        {
            return item
        } else {
            return Page(index: index, items: [])
        }
    }
    
    private func modifyPage(at index: Int, modification: (inout Page) -> Void) {
        let pageURL = self.pageURL(at: index)
        var page = self.page(at: index, removeFromCache: true)
        
        modification(&page)
        
        if page.items.isEmpty {
            try? FileManager.default.removeItem(at: pageURL)
        } else {
            cachedPages.append(page)
            try? encoder.encode(page).write(to: pageURL)
        }
        
        let toRemove = cachedPages.count - maxCachedPagesCount
        if toRemove > 0 {
            cachedPages.removeFirst(toRemove)
        }
    }
    
    private func releaseEmptyPages() {
        var page = self.info.lastPage
        while page > 0 && !FileManager.default.fileExists(atPath: self.pageURL(at: page).path) {
            page -= 1
            if page < self.info.lastPage - 20 { // in large backlogs let's persist the last page from time to time in case process is killed before stack checks all outdated backlog pages.
                self.info.lastPage = page
            }
        }
        self.info.lastPage = page
    }
    
    private let syncQueue: OperationQueue
    private func sync(changes: @escaping () -> Void) {
        if syncQueue == OperationQueue.current {
            changes()
        } else {
            syncQueue.addOperation {
                changes()
            }
            syncQueue.waitUntilAllOperationsAreFinished()
        }
    }
    
}

