//
//  Synchronizer+Sync.swift
//  
//
//  Created by Jan Mazurczak on 02/08/2020.
//

import Foundation

internal extension Synchronizer {
    
    func sync(changes: @escaping () -> Void) {
        if syncQueue == OperationQueue.current {
            changes()
        } else {
            var item: Synchronizable<Item>?
            var changed = false
            syncQueue.addOperation {
                let oldItem = self.merged.value
                changes()
                item = self.merged.value
                changed = oldItem != item
            }
            syncQueue.waitUntilAllOperationsAreFinished()
            if changed {
                subject.send(item?.item)
            }
        }
    }
    
}
