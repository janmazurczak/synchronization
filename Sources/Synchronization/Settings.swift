//
//  Settings.swift
//  
//
//  Created by Jan Mazurczak on 06/11/2019.
//

import Foundation

internal protocol AnySynchronizationSettings {
    associatedtype Item: Codable
}

public struct SynchronizationSettings<Item>: AnySynchronizationSettings where Item: Codable {
    
    public typealias CustomMerge = ([Synchronizable<Item>]) -> Synchronizable<Item>.Merged
    
    public enum MergeRule {
        case newer
        case local
        case anyRemote
        case custom(CustomMerge)
    }
    
    public enum LocalRoot {
        case documents
        case library
        case caches
        case appgroup(String)
        case custom(URL)
        internal var url: URL {
            switch self {
            case .documents: return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            case .library: return FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).first!
            case .caches: return FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
            case .appgroup(let groupname): return FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: groupname)!
            case .custom(let url): return url
            }
        }
    }
    
    public enum LocalPath {
        case `default`
        case custom([String])
        internal var pathComponents: [String] {
            switch self {
            case .default: return ["SyncItem"]
            case .custom(let path): return path
            }
        }
    }
    
    public let mergeRule: MergeRule
    public let modelVersion: Int
    public let localRoot: LocalRoot
    public let localPath: LocalPath
    internal func coordinatorURL(for identifier: String) -> URL {
        SynchronizationSettings<Item>.coordinatorURL(local: localRoot, path: localPath, identifier: identifier)
    }
    
    public static func defaultLocalRoot() -> LocalRoot {
        #if os(tvOS)
        return .caches
        #else
        return .library
        #endif
    }
    
    public init(at location: LocalRoot = defaultLocalRoot(), path: LocalPath = .default, mergeRule: MergeRule = .newer, modelVersion: Int = 0) {
        self.localRoot = location
        self.localPath = path
        self.mergeRule = mergeRule
        self.modelVersion = modelVersion
    }
    
    private static func coordinatorURL(local: LocalRoot, path: LocalPath, identifier: String) -> URL {
        var url = local.url
        let pathComponents = path.pathComponents + [identifier]
        for pathComponent in pathComponents {
            url = url.appendingPathComponent(pathComponent, isDirectory: true)
        }
        return url
    }
}
