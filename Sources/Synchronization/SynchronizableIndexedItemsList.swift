//
//  SynchronizableIndexedItemsList.swift
//
//  Created by Jan Mazurczak on 30/08/2019.
//  Copyright © 2019 Jan Mazurczak. All rights reserved.
//

import Foundation

///
/// Thanks to this you can add and delete identified items
/// without being worried about synchronisation.
///
/// Use the protocol like this:
///
///     struct Stories: SynchronizableIndexedItemsList {
///         typealias Info = Story
///         var items = [String : Item]()
///     }
///
/// and then provide merging:
///
///     extension Synchronization: SynchronizationSetup {
///
///         static var remotes: [SynchronizationRemote] { [] }
///
///         public func available(coordinators: inout AvailableCoordinators) {
///             coordinators.add(\.shelf)
///         }
///
///         var shelf: CoordinatorDefinition<Stories> {
///             .init(
///                 named: "Shelf",
///                 syncing: Self.remotes,
///                 with: .init(
///                     at: .library,
///                     path: .default,
///                     mergeRule: .custom(Stories.merging), // <- HERE
///                     modelVersion: 1
///                 )
///            )
///         }
///     }
///

public protocol SynchronizableIndexedItemsList: Codable {
    associatedtype ItemId: Codable, Hashable
    associatedtype Info: Codable
    var items: [ItemId: Item] { get set }
    typealias Item = SynchronizableIndexedItem<Info>
}

public struct SynchronizableIndexedItem<Info: Codable>: Codable {
    public init(_ info: Info? = nil, isPresent: Bool = true, updated: Date = .init()) {
        self.updated = updated
        self.presence = isPresent ? 1 : 0
        self.info = info
    }
    public var updated: Date
    public var presence: Int
    public var info: Info?
    enum CodingKeys: String, CodingKey {
        case updated = "u"
        case presence = "p"
        case info = "i"
    }
}

public extension SynchronizableIndexedItemsList {
    
    var presentItems: [ItemId: Item] {
        items.filter { $0.value.presence > 0 }
    }
    
    mutating func add(_ itemId: ItemId, with info: Info?, at date: Date = .init()) {
        items[itemId] = .init(info, updated: date)
    }
    mutating func delete(_ itemId: ItemId, at date: Date = .init()) {
        items[itemId] = .init(isPresent: false, updated: date)
    }
    
    /// Returns true if self was changed in order to merge.
    mutating func merge(from other: Self) -> Bool {
        var changed = false
        for (key, otherItem) in other.items {
            if
                let existing = items[key],
                existing.updated >= otherItem.updated
            {
                continue
            }
            items[key] = otherItem
            changed = true
        }
        return changed
    }
    
    /// Returns true if self was changed in order to merge.
    mutating func merge(from others: [Self]) -> Bool {
        var changed = false
        for other in others {
            if merge(from: other) { changed = true }
        }
        return changed
    }
    
    static func merging(_ syncItems: [Synchronizable<Self>]) -> Synchronizable<Self>.Merged {
        let newest: Synchronizable<Self>.Merged = syncItems
            .sorted { $0.modificationDate < $1.modificationDate }
            .last
            .map { .selected($0) } ?? .none
        let versions: [(version: Synchronizable<Self>, item: Self)] = syncItems.compactMap {
            guard let item = $0.item else { return nil }
            return (version: $0, item: item)
        }
        if versions.isEmpty {
            return newest
        }
        if versions.count == 1, let version = versions.first?.version {
            return .selected(version)
        }
        let mergedVersions: [(version: Synchronizable<Self>, item: Self, changed: Bool)] = versions.map { 
            let id = $0.version.uuid
            var item = $0.item
            let changed = item.merge(
                from: versions
                    .filter{ $0.version.uuid != id }
                    .map(\.item)
            )
            return (version: $0.version, item: item, changed: changed)
        }
        if let unchanged = mergedVersions.first(where: { !$0.changed }) {
            return .selected(unchanged.version)
        }
        return mergedVersions.first.map {
            .mixed($0.item, .init())
        } ?? newest
    }
    
}

public struct SynchronizableIndexedIdsList<ItemId: Codable & Hashable>: SynchronizableIndexedItemsList {
    
    public var items: [ItemId: Item]
    
    public struct Info: Codable {}
    
    public mutating func add(_ itemId: ItemId, at date: Date = .init()) {
        items[itemId] = .init(updated: date)
    }
    
}
