//
//  Synchronized.swift
//  
//
//  Created by Jan Mazurczak on 29/07/2020.
//

import Foundation
import Combine

@propertyWrapper
public final class Synchronized<Item: Codable> {
    
    private let coordinator: SynchronizationCoordinator<Item>
    public private(set) var synchronizer: Synchronizer<Item>?
    
    public init(_ path: KeyPath<Synchronization, Synchronization.CoordinatorDefinition<Item>>, _ identifier: String? = nil) {
        coordinator = Synchronization.coordinator(path)
        synchronizer = identifier.map(coordinator.synchronizer)
        bindSynchronizer()
    }
    
    public var wrappedValue: Item? {
        get { synchronizer?.item }
        set { synchronizer?.item = newValue }
    }
    
    public var projectedValue: Synchronized<Item> { self }
    
    /// Feeds with current version of the item or nil if item is not yet created nor fetched.
    public var publisher: AnyPublisher<Item?, Never> {
        subject
            .map {
                _ = self // Keep myself alive
                return $0
            }
            .eraseToAnyPublisher()
    }
    
    /// Falls to a regular publisher if item exists locally or after a successful fetch.
    /// Produced value being nil indicates that the item doesn't exist locally nor at any remote.
    public var fetchIfNeededPublisher: AnyPublisher<Item?, Error> {
        fetchPublisher(if: { _ in false })
    }
    
    /// Falls to a regular publisher if item exists locally or after a successful fetch.
    /// Produced value being nil indicates that the item doesn't exist locally nor at any remote.
    public func fetchPublisher(ifOlderThan expirationTime: TimeInterval, at date: Date = .init(), replaceErrorWithLocalVersionIfPresent: Bool = false) -> AnyPublisher<Item?, Error> {
        fetchPublisher( //TODO: make also 'every' timeInterval which will compare when was last fetch not when record changed
            if: { $0.modificationDate.addingTimeInterval(expirationTime) < date },
            replaceErrorWithLocalVersionIfPresent: replaceErrorWithLocalVersionIfPresent
        )
    }
    
    /// Falls to a regular publisher if item exists locally or after a successful fetch.
    /// Produced value being nil indicates that the item doesn't exist locally nor at any remote.
    public func fetchPublisher(if updateNeeded: (Synchronizable<Item>) -> Bool, replaceErrorWithLocalVersionIfPresent: Bool = false) -> AnyPublisher<Item?, Error> {
        Just(self)
            .setFailureType(to: Error.self)
            .map { synchronised -> AnyPublisher<Item?, Error> in
                if
                    let local = synchronised.synchronizer?.local,
                    !updateNeeded(local)
                {
                    return synchronised
                        .publisher
                        .setFailureType(to: Error.self)
                        .eraseToAnyPublisher()
                } else {
                    return Future<Synchronized<Item>, Error> { promise in
                        synchronised.fetch { error in
                            if let error = error {
                                if
                                    replaceErrorWithLocalVersionIfPresent,
                                    synchronised.synchronizer?.local != nil
                                {
                                    promise(.success(synchronised))
                                } else {
                                    promise(.failure(error))
                                }
                            } else {
                                promise(.success(synchronised))
                            }
                        }
                    }
                    .map { $0.publisher.setFailureType(to: Error.self) }
                    .switchToLatest()
                    .eraseToAnyPublisher()
                }
            }
            .switchToLatest()
            .eraseToAnyPublisher()
    }
    
    private let subject = CurrentValueSubject<Item?, Never>(nil)
    private var subjectBinding: AnyCancellable?
    private func bindSynchronizer() {
        subjectBinding?.cancel()
        subjectBinding = synchronizer?.publisher
            .sink { [weak subject] in subject?.send($0) }
    }
    
    public var loadedIdentifier: String? { synchronizer?.identifier }
    public func load(_ identifier: String?) {
        if let identifier = identifier {
            synchronizer = coordinator.synchronizer(identifier)
        } else {
            synchronizer = nil
        }
        bindSynchronizer()
    }
    
    public func modify(at modified: Date = Date(), modification: @escaping (inout Item?) -> Void) {
        synchronizer?.modify(at: modified, modification: modification)
    }
    
    public func update(item: Item?, modified: Date = Date()) {
        synchronizer?.update(item: item, modified: modified)
    }
    
    public func fetch(from remoteName: String? = nil, completion: ((Error?) -> Void)? = nil) {
        synchronizer?.fetch(from: remoteName, completion: completion)
    }
    
    public func push(to remoteName: String? = nil, completion: ((Error?) -> Void)? = nil) {
        synchronizer?.push(to: remoteName, completion: completion)
    }
}
