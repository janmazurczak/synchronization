//
//  Synchronizer+Merge.swift
//  
//
//  Created by Jan Mazurczak on 02/08/2020.
//

import Foundation

internal extension Synchronizer {
    
    func merge() {
        Logger.log?("Synchronizer \(identifier) is merging")
        let items = (remotes.map{ $0.item } + [local]).compactMap{ $0 }
        
        if items.isEmpty {
            Logger.log?("Synchronizer \(identifier) has no version")
            merged.send(nil)
            return
        }
        
        if items.count == 1, let item = items.first {
            Logger.log?("Synchronizer \(identifier) has one version present")
            merged.send(item)
            return
        }
        
        if Set(items.map{ $0.uuid }).count == 1, let item = items.first {
            Logger.log?("Synchronizer \(identifier) has all versions equal")
            merged.send(item)
            return
        }
        
        switch coordinator.settings.mergeRule {
        case .newer: merged.send(items.sorted(by: { $0.modificationDate > $1.modificationDate }).first)
        case .local: merged.send(local)
        case .anyRemote: merged.send(remotes.first?.item ?? local)
        case .custom(let merge):
            switch merge(items) {
            case .selected(let item): merged.send(item)
            case .mixed(let item, let date):
                merged.send(
                    .init(
                        item,
                        with: identifier,
                        modelVersion: coordinator.settings.modelVersion,
                        modificationDate: date
                    )
                )
            case .none: merged.send(nil)
            }
        }
    }
    
}
