//
//  Synchronizer+Local.swift
//  
//
//  Created by Jan Mazurczak on 02/08/2020.
//

import Foundation

internal extension Synchronizer {
    
    func loadLocal() {
        guard let data = try? Data(contentsOf: localURL) else { return }
        guard let item = try? JSONDecoder().decode(Synchronizable<Item>.self, from: data) else { return }
        local = item
        merge()
    }
    
    var isSaveNeeded: Bool {
        guard let merged = merged.value else { return false }
        if merged.uuid == local?.uuid { return false }
        return true
    }
    
    func save() {
        Logger.log?("Synchronizer \(identifier) is saving")
        guard let item = merged.value else { return }
        save(item)
        Logger.log?("Synchronizer \(identifier) saved")
    }
    
    func save(_ item: Synchronizable<Item>?) {
        guard let data = try? JSONEncoder().encode(item) else { return }
        try? data.write(to: localURL)
    }
    
}

