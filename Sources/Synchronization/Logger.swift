//
//  Logger.swift
//  
//
//  Created by Jan Mazurczak on 01/08/2020.
//

import Foundation

public class Logger {
    
    private init() {}
    public static let shared = Logger()
    
    public typealias DevelopmentLogger = (String) -> Void
    
    public static var log: DevelopmentLogger? {
        set { shared.log = newValue }
        get { shared.log }
    }
    
    #if DEBUG
    private var log: DevelopmentLogger? = { NSLog($0) }
    #else
    private var log: DevelopmentLogger? = nil
    #endif
    
}
