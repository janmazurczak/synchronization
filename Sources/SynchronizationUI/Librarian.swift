//
//  Librarian.swift
//  
//
//  Created by Jan Mazurczak on 24/01/2022.
//

import SwiftUI
import Combine
import Synchronization

open class Librarian<ShelfDefinition: Shelf>: ObservableObject {
    
    @Synchronized(Shelf.coordinatorPath) public var repository
    public typealias Shelf = ShelfDefinition
    
    private let loadingQueue = OperationQueue()
    private var subscripiton: AnyCancellable?
    
    // Display:
    @Published public private(set) var items: Shelf.DisplayItems? = nil
    @Published public private(set) var error: Shelf.DisplayItemsPublisher.Failure?
    
    private enum Result {
        case success(Shelf.DisplayItems)
        case failure(Shelf.DisplayItemsPublisher.Failure)
    }
    
    public init() {}
    
    open func load(at shelfId: String) {
        subscripiton?.cancel()
        items = nil
        error = nil
        
        $repository.load(shelfId)
        if repository == nil { repository = Shelf.emptyRepository }
        
        subscripiton = $repository
            .publisher
            .receive(on: loadingQueue)
            .map(Shelf.displayItemsPublisher(from:))
            .map {
                $0
                    .map { Result.success($0) }
                    .catch { Just(Result.failure($0)) }
            }
            .switchToLatest()
            .receive(on: OperationQueue.main)
            .sink { [weak self] result in
                withAnimation {
                    switch result {
                    case .success(let displayItems):
                        self?.error = nil
                        self?.items = displayItems
                    case .failure(let error):
                        self?.items = nil
                        self?.error = error
                    }
                }
            }
    }
    
}
