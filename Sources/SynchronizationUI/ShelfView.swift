//
//  ShelfView.swift
//  
//
//  Created by Jan Mazurczak on 24/01/2022.
//

import SwiftUI

@available(iOS 14.0, *)
public struct ShelfView<ShelfDefinition: Shelf, Content: View>: View {
    
    public typealias Shelf = ShelfDefinition
    
    public init(_ shelf: Shelf, @ViewBuilder content: @escaping (Librarian<Shelf>) -> Content) {
        self.shelfId = shelf.id
        self.content = content
    }
    
    let shelfId: String
    let content: (Librarian<Shelf>) -> Content
    
    @StateObject private var shelf = Librarian<Shelf>()
    
    public var body: some View {
        content(shelf)
            .environmentObject(shelf)
            .onAppear { shelf.load(at: shelfId) }
            .onChange(of: shelfId, perform: shelf.load(at:))
    }
    
}

@available(iOS 14.0, *)
public extension View {
    func shelf<S: Shelf>(_ shelf: S) -> some View {
        ShelfView(shelf) { _ in
            self
        }
    }
}
