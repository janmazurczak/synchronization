//
//  Shelf.swift
//  
//
//  Created by Jan Mazurczak on 24/01/2022.
//

import Combine
import Synchronization

///
/// Shelf protocol describes how should
/// **local copy of synchronized dto**
/// load into
/// **display item(s)**
///
/// Example:
///
///     struct Stories: SynchronizableIndexedItemsList {
///         typealias Info = StoryReference
///         var items = [UUID : Item]()
///     }
///
///     struct StoriesShelf: Shelf {
///         let id: String
///
///         static var emptyRepository: Stories { .init() }
///         static let coordinatorPath = \Synchronization.shelf
///
///         static func displayItemsPublisher(from repository: Stories?) -> AnyPublisher<[StoryReference], Never> {
///             Just(repository?.presentItems.compactMap(\.value.info) ?? []) //TODO: sorting
///                 .eraseToAnyPublisher()
///         }
///     }
///

public protocol Shelf {
    associatedtype Repository: Codable
    associatedtype DisplayItems
    associatedtype DisplayItemsFailure: Error
    typealias DisplayItemsPublisher = AnyPublisher<DisplayItems, DisplayItemsFailure>
    
    static var coordinatorPath: KeyPath<Synchronization, Synchronization.CoordinatorDefinition<Repository>> { get }
    var id: String { get }
    
    static var emptyRepository: Repository { get }
    static func displayItemsPublisher(from repository: Repository?) -> DisplayItemsPublisher
}
