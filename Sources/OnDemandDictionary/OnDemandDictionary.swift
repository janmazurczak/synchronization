//
//  SyncDictionary.swift
//
//  Created by Jan Mazurczak on 15/09/2019.
//  Copyright © 2019 Jan's Games. All rights reserved.
//

import Foundation

public struct OptionalContainer<Item> {
    var item: Item?
}

public class OnDemandDictionary<Identifier: Hashable, Item>: SyncDictionary<Identifier, Item, OptionalContainer<Item>> {
    
    public init() {
        super.init(itemPath: \.item, storageCreator: OptionalContainer.init)
    }
    
}

