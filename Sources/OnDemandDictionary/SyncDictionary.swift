//
//  SyncDictionary.swift
//  
//
//  Created by Jan Mazurczak on 01/08/2020.
//

import Foundation

public class SyncDictionary<Identifier: Hashable, Item, StoredItem> {
    
    let itemPath: WritableKeyPath<StoredItem, Item?>
    let storageCreator: (Item) -> StoredItem
    
    public init(
        itemPath: WritableKeyPath<StoredItem, Item?>,
        storageCreator: @escaping (Item) -> StoredItem)
    {
        self.itemPath = itemPath
        self.storageCreator = storageCreator
        self.syncQueue = OperationQueue()
        self.syncQueue.maxConcurrentOperationCount = 1
        self.dictionary = [:]
    }
    
    /// Retrieves existing item or creates new one. This method ensures creation on the same thread as checking if item already exists. Use this method instead of checking if item doesn't exist via subscript in order to create it.
    public func retrieveItem(for identifier: Identifier, or create: @escaping (Identifier) -> Item) -> Item {
        var result: Item?
        sync {
            if let existing = self.dictionary[identifier]?[keyPath: self.itemPath] {
                result = existing
            } else {
                let newItem = create(identifier)
                result = newItem
                self.dictionary[identifier] = self.storageCreator(newItem)
            }
        }
        return result!
    }
    
    public func syncOnItem(with identifier: Identifier, syncedAction: @escaping (inout Item?) -> Void) {
        sync {
            var item = self.dictionary[identifier]?[keyPath: self.itemPath]
            syncedAction(&item)
            self.dictionary[identifier]?[keyPath: self.itemPath] = item
        }
    }
    
    public func syncOnEach(syncedAction: @escaping (inout Item?) -> Void) {
        sync {
            for identifier in self.dictionary.keys {
                var item = self.dictionary[identifier]?[keyPath: self.itemPath]
                syncedAction(&item)
                self.dictionary[identifier]?[keyPath: self.itemPath] = item
            }
        }
    }
    
    public subscript (identifier: Identifier) -> Item? {
        get {
            var result: Item?
            sync {
                result = self.dictionary[identifier]?[keyPath: self.itemPath]
            }
            return result
        }
        set {
            sync {
                if let item = newValue {
                    self.dictionary[identifier] = self.storageCreator(item)
                } else {
                    self.dictionary.removeValue(forKey: identifier)
                }
            }
        }
    }
    
    internal func sync(operation: @escaping () -> Void) {
        syncQueue.addOperation(operation)
        syncQueue.waitUntilAllOperationsAreFinished()
    }
    internal let syncQueue: OperationQueue
    internal var dictionary: [Identifier : StoredItem]
}

